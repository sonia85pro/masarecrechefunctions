
let db = {
    users: [
        {
        userId: 'Mo0SbYYZlpNUDohztTx5',
        email: 'user@email.com',
        handle: 'user',
        createdAt: '2019-12-05T01:19:13.327',
        imageUrl: 'gs://fir-app-bcf0d.appspot.com/no-img.png',
        bio: 'Hello, my name is user, nice to meet you',
        website: 'https://user.com',
        location: 'Lonodn, UK'
    }
],
    screams: [{
        userHandle: 'user',
        body: 'This is a sample scream',
        createdAt: '2019-12-05T01:19:13.327',
        likeCount: 5,
        commentCount: 3
    }
],
    comments: [
        {
        userHandle: 'user',
        screamId: 'PUcHFfNrYjBd9H4UR8Qt ',
        body: 'nice one mate!',
        createdAt: '2019-12-05T01:19:13.327'
    }
],
    notifications: [
        {
        recipient: 'user',
        sender: 'sonia',
        read: 'true | false',
        screamId: 'PUcHFfNrYjBd9H4UR8Qt ',
        type: 'like | comment',
        createdAt: '2019-12-05T01:19:13.327'
    }
]
};
const userDetails = {
    // Redux data
    credentials: {
        userId: 'Mo0SbYYZlpNUDohztTx5',
        email: 'user@email.com',
        handle: 'user',
        createdAt: '2019-12-05T01:19:13.327',
        imageUrl: 'image/dsfsdkfghskdfgs/dgfdhfgdh',//adress image
        bio: 'Hello, my name is user, nice to meet you',
        website: 'https://user.com',
        location: 'Lonodn, UK'
    },
    likes: [
        {
            userHandle: 'user',
            screamId: 'PUcHFfNrYjBd9H4UR8Qt '
        },
        {
            userHandle: 'user',
            screamId: 'PUcHFfNrYjBd9H4UR8Qt '// changer screamId
        }
    ]
};